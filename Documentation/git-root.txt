git-root(8)
===========

NAME
----
git-root - Use the system configuration git repository

SYNOPSIS
--------
[verse]
'git root' COMMAND [ARGS]

DESCRIPTION
-----------
The `git root FOO` command is an abbreviation for
`git --git-dir=/var/lib/root.git --work-tree=/ FOO`.
It is intended for use when managing the system's configuration with
linkgit:git-deploy[1] when that configuration spans multiple top-level
directories. The repository is kept out of the way in order to stop
normal git commands finding `/.git` and thinking they are inside the
root working tree.

The `git root init` command automatically adds the `--shared=0700`
option.

SEE ALSO
--------
linkgit:git-deploy[5]

Author
------
Written by Tony Finch <dot@dotat.at>
// You may do anything with this, at your own risk.
// http://creativecommons.org/publicdomain/zero/1.0/

GIT
---
Part of the linkgit:git[1] suite
