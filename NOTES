To Do
-----

* Check documentation markup.

* Replace --debug with GIT_TRACE.


Safely updating files
---------------------

The basic strategy is to write the new file to a temporary path, fix
up ownerships and permissions, then rename to the final place. The
first part can be done using `git checkout-index --temp` however
there's a minor problem that it puts all the temporary files in the
top of the working tree.

A major goal of `git deploy` and `git root` is to be able to manage
whole system configurations, which means the working tree is going to
span multiple filesystems. So simply renaming into place is sometimes
going to fail. If we get an EXDEV then we will have to copy the file
into its destination directory (taking care about permissions) before
renaming it into place. Somewhat less than ideal.

The other risk is that the tempfile spew in the root directory will
fill the filesystem, when the checkout would succeed if the tempfiles
were put directly in their destination directories.


Better plumbing support
-----------------------

For this initial version of git-deploy, the aim is to avoid changes to
the git core. If that becomes feasible, what changes would be appropriate?

It seems reasonable to keep the details of how permissions and
ownerships separate from the core, because they are likely to change,
e.g. support for extended attributes, differences between operating
systems. So it should be done by a hook or porcelain. Either way it
should be invoked once to fix the permissions of all files in one go,
not many times to fix one file at a time, to minimize forks/execs. The
quantity of information that needs to be transferred from the plumbing
suggests that the permissions should be set by porcelain not by a
hook.

So if git-deploy's current design is roughly right, what can the
plumbing do to make the porcelain's job easier? I think an option for
git-checkout-index that's similar to the --temp option but closer to a
normal checkout will be enough.

Firstly, we want to avoid the EXDEV problem, so new files should be
written to temporary locations in the same directory as their final
location.

At the moment git-deploy has to duplicate too much of the checkout
logic, in particular coping with file/directory conflicts. The
plumbing should remove directories that are in the way, and create any
missing directories.

With that, all git-deploy has to do is load and set permissions, then
rename the files into place.


The push problem
----------------

One of the workflows I want to support is using `git push` to transfer
an updated repository from the master configuration management server
to the live repository on the target server, then running `git deploy`
on the target to update the working directory. In many cases you
cannot transfer the repository using `git fetch`, for example when the
target server is in a DMZ firewalled off from the master server. The
problem is that it is a mistake to push to a branch that is currently
checked out, and in fact future versions of git will prevent this.
However we want the target server to follow one of the branches pushed
to it, and we want to do so without using merges because they would
disrupt the live working directory.

So the question is what to do with the HEAD ref on the target to allow
you to push to the target, and so that you can use the HEAD reflog to
roll back.

One option is for `git deploy` to turn the checkout into a detached
head. This should make everything work as expected, modulo the usual
caveats about commits to detached heads. However it is rather weird,
and sort-of contradicts the usual advice against pushing to a
checked-out branch.

One alternative might be to create a special branch for the checkout.
The branch should never be committed to, so it can be trivially
fast-forwarded when `git deploy` updates the working tree - and `git
deploy` can check that this is the case. This has a similar effect to
using a detached head, except that the user needs to choose a magic
branch name, and there is extra sanity checking.


Questions
---------

* If a file is not mentioned in the current tree but is present on
  disk (i.e. it is untracked) and it is mentioned in the target, zap
  the old file or not? Maybe require the file to be removed by the
  .gitdeploy script?

* What to do if a path exists in both revisions but is a directory
  in one and not in the other? In this case the new one cannot be
  renamed into place.

* Maybe need a umask setting in the configuration file, just to be
  sure? Or we could just make paranoid admins set it in a wrapper
  script. Similarly for ensuring we are being run by the right user?

* Renaming files into place implies we need write access to all
  directories. We can assume that it'll always work for root. For
  non-privileged users, should we require that directories always have
  the write bit set? Or leave it to the admin as above.

* Do we need to do anything special about nested .git repositories?
  Main use case would be keeping your home directory in git. Have a
  look at gibak.

* Do we need another script to support repeated merges from the set
  of configuration modules that is composed to make a machine's
  configuration? See also git-subtree.

* Do we need a pre-deploy hook as well as a post-deploy hook?

* Allow globbing in .gitdeploy files? Spaces and other weird
  characters in filenames?

* Can we avoid loading .gitdploy files we don't need?


etckeeper vs. git-deploy
------------------------

etckeeper is not designed to update the working tree in a way that is safe
on live servers. git-deploy renames files into place so they are always
available for servers that repeatedly re-read them. git-deploy also avoids
permissions races, since it writes files with mode 0700 (using git
checkout-index --temp) and their permissions are fixed before they are
renamed.

etckeeper and git-deploy store metadata formatted like a shell script.
There is one etckeeper script at the top of the working tree; it
depends on a non-standard auxiliary command and is not designed to be
edited. git-deploy can have scripts at any level of the hierarchy;
they use a tiny subset of standard shell commands, and may be edited
by the user.

etckeeper is designed to manage /etc only, whereas git-deploy is
general-purpose. I plan to use it to manage /etc, /opt, and other
miscellaneous bits and bobs.

etckeeper works with many version control systems whereas git-deploy is
only for git.

etckeeper hooks into your package manager. git-deploy doea not.

Basically etckeeper seems to be designed to keep a record of changes that
are made on a working machine, whereas git-deploy is designed to install
changes on a working machine that were prepared elsewhere.


Why not puppet?
---------------

I dislike puppet for two main reasons, one small and one big. There
are also a couple of lesser reasons.

The small reason is that I dislike configuration files that write
configuration files. (The very worst example of this that I have seen
is AutoYaST.) Puppet does not have such a bad case of this, since it
lets you do a lot of the heavy lifting with literal configuration
files or simple templated files. However its configuration layout is
completely divorced from the layout on the final machine, which means
that in order to be usable, puppet MUST have strong debugging tools
for tracing back final configurations to their sources. I am also
concerned about puppet's approach to cross-platform support. I prefer
to configure major cross-platform packages (e.g. apache, bind) in a
platform-agnostic manner, whereas puppet seems to indulge gratuitous
platform variations. (I have not checked out these aspects of
puppet's functionality in enough detail to know if my FUD is
unwarranted.)

The big reason is that I think puppet's architecture is fundamentally
wrong. I prefer a master-slave architecture in which all actions are
driven by the master and trust is hierarchial, so root on the target
servers is less trusted than unprivileged accounts on the master.
Puppet has a number of problems, many of which are caused by a
client-server architecture in which the puppet master trusts its
clients too much.

Puppet has its own authentication system. It can't use your existing
ssh setup, for example. This increases the attack surface at the very
core of your system.

Clients pull their configurations from the server according to their
own schedule. This means the puppet master must implement access
controls, which further complicates the security architecture. What
is worse, the repository on the server must always contain working
configurations.

Clients have some control over their configurations (e.g. they can
choose their "environment"), whereas this should be determined
entirely by the server.

The master does not determine when changes are pushed to target servers.
The puppet client has to implement a work-around to avoid thundering
herd problems. Puppet isn't designed for the admin to push changes on
demand. Instead its clients waste energy polling.

I believe it's possible to make puppet work in something resembling
master-slave mode but this is not well documented, and it's generally
unwise to force a program to work outside its design.

I'm also not entirely convinced by puppet's testing story. For
example, http://reductivelabs.com/trac/puppet/wiki/BranchTesting seems
rather heavy, and environments are backwards. It looks like the puppet
way would be to explicitly designate which machines fall into which
stage of a phased testing/rollout procedure with a separate
configuration for each stage, and when rolling out a change you would
have to merge the config change into each stage one after the other.
The more lightweight procedure I have in mind for git-deploy would be
to have one configuration tree, and implement a phased rollout by
selectively deploying it on the appropriate subsets of the machines in
stages. Because the configuration is pushed out under control of the
master, the target machines cannot unexpectedly pull a configuration
before the admin is ready.

It must be said that my original goal was to make a fairly incremental
improvement to our configuration management practices. That's less
true now because git-deploy will require a change of VCS, which is not
a very incremental change! However our basic approach remains the
same. Puppet takes a rather different (much more abstract) approach.
Perhaps I am too set in my ways?
